# Update packages
yum update -y

# Package/repo needed for python3
# sudo yum -y install https://centos7.iuscommunity.org/ius-release.rpm
yum install -y epel-release

# packages insalation
yum install httpd vim wget nginx java python34 -y

#fix java
yum remove -y java
yum install -y java-1.8.0-openjdk


# start server
service httpd start

# Jenkins installation
wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo
rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key
yum install -y jenkins

#start jenkins
# TODO for testing
sudo service jenkins start/stop/restart
sudo chkconfig jenkins on

#disable firewall
firewall-cmd --permanent --new-service=jenkins
firewall-cmd --permanent --service=jenkins --set-short="Jenkins Service Ports"
firewall-cmd --permanent --service=jenkins --set-description="Jenkins service firewalld port exceptions"
firewall-cmd --permanent --service=jenkins --add-port=8080/tcp
firewall-cmd --permanent --add-service=jenkins
firewall-cmd --zone=public --add-service=http --permanent
firewall-cmd --reload