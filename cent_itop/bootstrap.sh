# Update packages
yum update -y

# Python required package 
yum install -y epel-release

# installe packages conf 
yum install php php-mysql python34 mariadb-server sos httpd vim wget unzip mysql policycoreutils-python mysql-server php php-mysql php-mcrypt php-xml php-cli php-soap php-ldap graphviz -y

# start server
# TODO Make it work
service httpd start

#go to /var/www/html
cd /var/www/html

# get the files
# TODO add link to a variable
wget 'https://sourceforge.net/projects/itop/files/itop/2.4.1/iTop-2.4.1-3714.zip'

# unzip the files
# TODO add zip to a variable
unzip iTop-2.4.1-3714.zip

# TODO For testing
#DB Install
wget https://dev.mysql.com/get/mysql57-community-release-el7-9.noarch.rpm
sudo rpm -ivh mysql57-community-release-el7-9.noarch.rpm
sudo yum install -y mysql-server
rm mysql57-community-release-el7-9.noarch.rpm

#run mysql
sudo systemctl start mysqld


#conf mysql
# [vagrant@localhost ~]$ sudo mysql_secure_installation^C
# [vagrant@localhost ~]$ sudo grep 'temporary password' /var/log/mysqld.log
# 2018-06-17T08:13:40.003923Z 1 [Note] A temporary password is generated for root@localhost: DE4QQCY*cM:c


# asd
# setsebool -P allow_httpd_anon_write 1
# setsebool -P allow_httpd_sys_script_anon_write 1
# setsebool -P httpd_can_network_connect_db 1
# setsebool -P httpd_anon_write 1
# setsebool -P httpd_run_stickshift 0 
# setsebool -P httpd_can_connect_ldap 0
# setsebool -P httpd_can_network_connect 1
# setsebool -P httpd_can_network_connect_cobbler 1

# privilagess required
sudo chmod 0755 -R /var/www
sudo chmod g+s -R /var/www
sudo chown -R apache /var/www/