# location of file
path = "/etc/hosts"

# itop line
itop_line = " \n 192.168.33.10 itop.example.com \n "

#jenkins line
jenkins_line = " \n 192.168.33.11 jenkins.example.com \n "

with open(path, 'a') as file:
    file.write(itop_line)
    file.write(jenkins_line)
